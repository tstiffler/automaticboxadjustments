﻿using NLog;
using AutomaticBoxAdjustments.Classes;
using AutomaticBoxAdjustments.GPService;
using NTSupply.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace AutomaticBoxAdjustments
{
    public class BoxAdjustment
    {
        /*NLog*/
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private NFS_ShopFloorDAL NFS_ShopFloorDAL;
        private bool liveMode = false;
        private BoxAdjustmentsBLL boxAdjustmentBLL = null;

        public BoxAdjustment(bool LiveMode)
        {
            liveMode = LiveMode;
            boxAdjustmentBLL = new BoxAdjustmentsBLL(liveMode);
        }

        public void Run(DateTime? StartDate, DateTime? EndDate)
        {
            DateTime startDate;
            DateTime endDate;
            bool alreadyRanToday = false;
            List<BoxHead> unAdjustedFilters = new List<BoxHead>();
            List<BoxHead> adjustedFilters = new List<BoxHead>();
            List<BoxHead> nonUpdatedFilters = new List<BoxHead>();
            List<BoxHead> updatedFilters = new List<BoxHead>();

            // Get boxes to adjust
            try
            {
                unAdjustedFilters = boxAdjustmentBLL.GetUnAdjustedBoxes(StartDate, EndDate);
                logger.Info(unAdjustedFilters.Count() + " boxes to be adjusted.");
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }

            // Make the adjustment in GP
            try
            {
                if (unAdjustedFilters.Count() > 0)
                {
                    adjustedFilters = boxAdjustmentBLL.CreateBoxAdjustment(unAdjustedFilters);

                    foreach (BoxHead box in adjustedFilters)
                    {
                        if (box.BoxAdjusted == false)
                        {
                            logger.Error("Error adding adjustment to box " + box.BoxNumber + "; count = " +
                                         box.BoxCount + ".");
                        }
                    }
                }
                else
                {
                    logger.Error("There were no boxes to be adjusted on " + DateTime.Today);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
            
            // Update FiltersBuilt table to show which boxes were adjusted
            try
            {
                nonUpdatedFilters = adjustedFilters.Where(x => x.BoxAdjusted == true).ToList();
                updatedFilters = boxAdjustmentBLL.UpdateBoxesAdjusted(nonUpdatedFilters);

                if (updatedFilters.Count() != nonUpdatedFilters.Count())
                {
                    logger.Error("Counts for adjusted boxes do not match counts for unadjusted boxes.");
                }

                foreach (BoxHead nonUpdatedFilter in nonUpdatedFilters)
                {
                    foreach (BoxHead updatedFilter in updatedFilters)
                    {
                        if (nonUpdatedFilter.BoxNumber == updatedFilter.BoxNumber)
                        {
                            if (nonUpdatedFilter.BoxDetails.Count() != updatedFilter.BoxDetails.Count())
                            {
                                logger.Error("Counts for individual adjusted FiltersBuiltIDs do not match counts for individual adjusted FilterBuiltIDs.");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }
        }

        private void sendErrorEmail(DateTime startDate, DateTime endDate, bool liveMode)
        {
            string date = DateTime.Now.ToString();
            string server = "smtp.office365.com";
            string bodyHTML = "Error creating custom filter sales invoice.";
            string recipients = "";

            MailMessage message = new MailMessage();
            message.From = new MailAddress("cs_noreply@ntsupply.com");
            message.Subject = "ERROR - Error creating custom filter sales invoice.";

            try
            {
                bodyHTML = "Error creating custom filter sales invoice.  Start date: " + startDate.ToString() +
                           "; end date: " + endDate + "; live mode: " + liveMode.ToString();
            }
            catch (Exception ex)
            {
                logger.Error("Error creating error email: " + ex.Message);
            }

            message.Body = bodyHTML;
            message.IsBodyHtml = true;

            recipients = "tstiffler@discountfilters.com";
            message.To.Add(recipients);

            SmtpClient client = new SmtpClient(server);
            client.EnableSsl = true;
            client.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("cs_noreply@ntsupply.com", "P@55w0rd");

            try
            {
                //Sends the email
                client.Send(message);
            }
            catch (Exception ex)
            {
                logger.Error("Error sending error e-mail: " + ex);
            }
        }
    }
}
