﻿using NLog;
using NTSupply.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomaticBoxAdjustments
{
    class Program
    {
        /*NLog*/
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            //
            //
            // This is a scheduled task that runs monthly on SQL03. 
            // This takes all the custom filters from purchase orders in the month and makes a sales invoice in GP.
            //
            // Parameters / Arguments:
            //  [1] To process in live mode, pass the first parameter as 1 or true
            //  [2] To process a day with overriding the ran today check, pass the second parameter as the start date for this run
            //  [3] To process a day with overriding the ran today check, pass the third parameter as the end date for this run. 
            //      If parameter 2 is passed and not parameter 3, then parameter 3 will default to parameter 2.
            //
            //

            try
            {
                bool liveMode = false;
                bool overrideRunChecks = false;
                DateTime? startDate = null;
                DateTime? endDate = null;

                logger.Info(Environment.NewLine + "Passed in arguments: " + string.Join(",", args));

                //If any parameters are passed, then handle them
                if (args.Length > 0)
                {
                    //If the first parameter is true or 1, then process in live mode. Else, process in dev mode.
                    if (ConvertType.ToBoolean(args[0]) == true || ConvertType.ToInteger(args[0]) == 1)
                    {
                        liveMode = true;
                    }
                    logger.Info("Program.cs live mode: " + liveMode);

                    //If the second parameter is passed, then this is the start date of this run.
                    if (args.Length >= 2 && args[1] != null)
                    {
                        startDate = ConvertType.ToNullableDateTime(args[1]);
                    }
                    logger.Info("Program.cs start date: " + startDate);

                    //If the third parameter is passed, then this is the end date of this run.
                    if (args.Length >= 3 && args[2] != null)
                    {
                        endDate = ConvertType.ToNullableDateTime(args[2]);
                    }
                    logger.Info("Program.cs end date: " + endDate);
                }

                //Handles the Sales Invoice Creation
                BoxAdjustment BoxAdjustment = new BoxAdjustment(liveMode);
                BoxAdjustment.Run(startDate, endDate);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}
