﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace NTSupply.Utilities
{
    public static class ConvertType
    {
        public static string ToNullableString(object StringObject)
        {
            if (StringObject == null || String.IsNullOrEmpty(StringObject.ToString()))
            {
                return null;
            }
            else
            {
                if (String.IsNullOrEmpty(StringObject.ToString()))
                {
                    return null;
                }
                else
                {
                    return StringObject.ToString();
                }
            }
        }

        public static Nullable<int> ToNullableInteger(object IntegerObject)
        {
            if (IntegerObject == null || String.IsNullOrEmpty(IntegerObject.ToString()))
            {
                return null;
            }
            else
            {
                int integerValue;

                //If the Integer Object Contains a Period, then take only the text before the Period
                if (IntegerObject.ToString().Contains("."))
                {
                    IntegerObject = IntegerObject.ToString().Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries)[0];
                }

                if (int.TryParse(IntegerObject.ToString(), out integerValue))
                {
                    return integerValue;
                }
                else
                {
                    return null;
                }
            }
        }

        public static int ToInteger(object IntegerObject)
        {
            if (IntegerObject == null || String.IsNullOrEmpty(IntegerObject.ToString()))
            {
                return 0;
            }
            else
            {
                int integerValue;

                //If the Integer Object Contains a Period, then take only the text before the Period
                if(IntegerObject.ToString().Contains("."))
                {
                    IntegerObject = IntegerObject.ToString().Split(new string[] { "." }, StringSplitOptions.RemoveEmptyEntries)[0];
                }

                if (int.TryParse(IntegerObject.ToString(), out integerValue))
                {
                    return integerValue;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static Nullable<double> ToNullableDouble(object DoubleObject)
        {
            if (DoubleObject == null || String.IsNullOrEmpty(DoubleObject.ToString()))
            {
                return null;
            }
            else
            {
                double doubleValue;
                if (double.TryParse(DoubleObject.ToString(), out doubleValue))
                {
                    return doubleValue;
                }
                else
                {
                    return null;
                }
            }
        }

        public static double ToDouble(object DoubleObject)
        {
            if (DoubleObject == null || String.IsNullOrEmpty(DoubleObject.ToString()))
            {
                return 0;
            }
            else
            {
                double doubleValue;
                if (double.TryParse(DoubleObject.ToString(), out doubleValue))
                {
                    return doubleValue;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static DateTime ToDateTime(object DateTimeObject)
        {
            if (DateTimeObject == null || String.IsNullOrEmpty(DateTimeObject.ToString()))
            {
                return DateTime.MinValue;
            }
            else
            {
                DateTime dateTimeValue;
                if (DateTime.TryParse(DateTimeObject.ToString(), out dateTimeValue))
                {
                    return dateTimeValue;
                }
                else
                {
                    return DateTime.MinValue;
                }
            }
        }

        public static Nullable<DateTime> ToNullableDateTime(object DateTimeObject)
        {
            if (DateTimeObject == null || String.IsNullOrEmpty(DateTimeObject.ToString()))
            {
                return null;
            }
            else
            {
                DateTime dateTimeValue;
                if (DateTime.TryParse(DateTimeObject.ToString(), out dateTimeValue))
                {
                    return dateTimeValue;
                }
                else
                {
                    return null;
                }
            }
        }

        public static Boolean ToBoolean(object BooleanObject)
        {
            if (BooleanObject == null || String.IsNullOrEmpty(BooleanObject.ToString()))
            {
                return false;
            }
            else
            {
                Boolean booleanValue;
                if (Boolean.TryParse(BooleanObject.ToString(), out booleanValue))
                {
                    return booleanValue;
                }
                else
                {
                    return false;
                }
            }
        }

        public static Nullable<Boolean> ToNullableBoolean(object BooleanObject)
        {
            if (BooleanObject == null || String.IsNullOrEmpty(BooleanObject.ToString()))
            {
                return null;
            }
            else
            {
                Boolean booleanValue;
                if (Boolean.TryParse(BooleanObject.ToString(), out booleanValue))
                {
                    return booleanValue;
                }
                else
                {
                    return null;
                }
            }
        }

        public static decimal ToDecimal(object DecimalObject)
        {
            if (DecimalObject == null || String.IsNullOrEmpty(DecimalObject.ToString()))
            {
                return 0;
            }
            else
            {
                decimal DecimalValue;
                if (decimal.TryParse(DecimalObject.ToString(), out DecimalValue))
                {
                    return DecimalValue;
                }
                else
                {
                    return 0;
                }
            }
        }

        public static Nullable<decimal> ToNullableDecimal(object DecimalObject)
        {
            if (DecimalObject == null || String.IsNullOrEmpty(DecimalObject.ToString()))
            {
                return null;
            }
            else
            {
                decimal DecimalValue;
                if (Decimal.TryParse(DecimalObject.ToString(), out DecimalValue))
                {
                    return DecimalValue;
                }
                else
                {
                    return null;
                }
            }
        }

        public static Nullable<decimal> FromFractionToNullableDecimal(object DecimalObject)
        {
            if (DecimalObject == null || String.IsNullOrEmpty(DecimalObject.ToString()))
            {
                return null;
            }
            else
            {
                //Removes all non-numeric characters except for forward slashes
                DecimalObject = Regex.Replace(DecimalObject.ToString(), @"[^a-zA-Z0-9 \-\.\/]", String.Empty).Trim();
                DecimalObject = DecimalObject.ToString().Replace("-", " ");

                decimal DecimalValue;
                if (Decimal.TryParse(DecimalObject.ToString(), out DecimalValue))
                {
                    return DecimalValue;
                }
                else
                {
                    try
                    {
                        string[] split = DecimalObject.ToString().Split(new char[] { ' ', '/' });

                        if (split.Length == 2 || split.Length == 3)
                        {
                            int a, b;

                            if (int.TryParse(split[0], out a) && int.TryParse(split[1], out b))
                            {
                                if (split.Length == 2)
                                {
                                    return (decimal)a / b;
                                }

                                int c;

                                if (int.TryParse(split[2], out c))
                                {
                                    return a + (decimal)b / c;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    { 
                        //Do Nothing 
                    }

                    return null;
                }
            }
        }
    }
}
