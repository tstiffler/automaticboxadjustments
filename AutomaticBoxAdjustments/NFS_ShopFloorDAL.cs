﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using AutomaticBoxAdjustments.Classes;
using NLog;

namespace AutomaticBoxAdjustments
{
    public class NFS_ShopFloorDAL
    {
        private string NFSShopFloorConnectionString = "";

        /*NLog*/
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public NFS_ShopFloorDAL(bool LiveMode)
        {
            try
            {
                if (LiveMode)
                {
                    NFSShopFloorConnectionString = AutomaticBoxAdjustments.Properties.Settings.Default.NFSShopFloorConnectionString;
                }
                else
                {
                    NFSShopFloorConnectionString = AutomaticBoxAdjustments.Properties.Settings.Default.NFSShopFloorDevConnectionString;
                }

                logger.Info("DAL. Live mode: " + LiveMode);
                logger.Info("DAL. Connection string: " + NFSShopFloorConnectionString);
            }
            catch (Exception ex)
            {
                logger.Error("DAL. Error setting connection string: " + ex.Message);
                throw ex;
            }
        }

        private IDbConnection createConnection()
        {
            try
            {
                if (string.IsNullOrEmpty(NFSShopFloorConnectionString))
                    throw new Exception("The NFSShopFloor Connection string is not set. Please correct.");
            }
            catch (Exception ex)
            {
                logger.Error("DAL. Error creating connection: " + ex.Message);
                throw ex;
            }

            return new SqlConnection(NFSShopFloorConnectionString);
        }

        public List<BoxHead> GetUnAdjustedRecords(DateTime StartDate, DateTime EndDate)
        {
            string sql = "";
            List<BoxHead> unAdjustedRecords = new List<BoxHead>();
            List<Box> unAdjustedDetails = new List<Box>();

            sql = @"SELECT fb.BoxNumber
	                , bom.BoxNumber AS BomBoxNumber
	                , bm.BoxCount AS PackingBoxCount
	                , bm.BomBoxCount
	                , fb.FilterItemID
	                , CASE
		                 WHEN fb.NumberOfFiltersBuilt <> 6
		                 THEN insn.mfg_name + ' ' + CAST(fb.NumberOfFiltersBuilt AS VARCHAR)
		                 ELSE insn.mfg_name
	                  END AS BaseModelCode_Calculated
	                , COUNT(*) AS BoxCount
                FROM
	                FiltersBuilt fb
	                LEFT JOIN dishbooks_dev.dbo.items_not_shipped_by_nts insn ON fb.FilterItemID = insn.item_id
	                LEFT JOIN BaseModels bm ON CASE
							                 WHEN fb.NumberOfFiltersBuilt <> 6
							                 THEN insn.mfg_name + ' ' + CAST(fb.NumberOfFiltersBuilt AS VARCHAR)
							                 ELSE insn.mfg_name
						                  END = bm.Code
	                LEFT JOIN Boxes b ON bm.BoxID = b.BoxID
	                LEFT JOIN Boxes bom ON bm.BomBoxID = bom.BoxID
                WHERE  BuiltDate >= @startDate
	                  AND BuiltDate <= DATEADD(dd, 1, @endDate)
	                  AND BoxAdjusted = 0
	                  AND ShippingLabelScanned = 1
	                  AND Source = 'OnDemand'
                GROUP BY fb.BoxNumber
	                  , bom.BoxNumber
	                  , bm.BoxCount
	                  , bm.BomBoxCount
	                  , fb.FilterItemID
	                  , CASE
		                   WHEN fb.NumberOfFiltersBuilt <> 6
		                   THEN insn.mfg_name + ' ' + CAST(fb.NumberOfFiltersBuilt AS VARCHAR)
		                   ELSE insn.mfg_name
	                    END";

            try
            {
                using (IDbConnection DB = createConnection())
                {
                    unAdjustedRecords = DB.Query<BoxHead>(sql,
                        new
                        {
                            @startDate = StartDate,
                            @endDate = EndDate
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error("NFS_ShopFloorDAL. Error getting box counts: " + ex.Message);
                throw;
            }

            try
            {
                unAdjustedDetails = GetUnAdjustedDetails(StartDate, EndDate);

                foreach (BoxHead head in unAdjustedRecords)
                {
                    head.BoxDetails = new List<Box>();
                    foreach (Box detail in unAdjustedDetails)
                    {

                        if (detail.BoxNumber == head.BoxNumber)
                        {
                            head.BoxDetails.Add(detail);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("NFS_ShopFloorDAL.  Error getting box details: " + ex.Message);
                throw;
            }

            return unAdjustedRecords;
        }

        public List<Box> GetUnAdjustedDetails(DateTime StartDate, DateTime EndDate)
        {
            string sql = "";
            List<Box> unAdjustedRecords = new List<Box>();

            sql = @"SELECT ID AS FiltersBuiltID, BoxNumber
                    FROM   FiltersBuilt
                    WHERE  BuiltDate >= @startDate
	                      AND @endDate <= DATEADD(dd, 1, @endDate)
	                      AND BoxAdjusted = 0
	                      AND ShippingLabelScanned = 1
	                      AND Source = 'OnDemand'
                    ";

            try
            {
                using (IDbConnection DB = createConnection())
                {
                    unAdjustedRecords = DB.Query<Box>(sql,
                        new
                        {
                            @startDate = StartDate,
                            @endDate = EndDate
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error("DAL. Error getting box counts: " + ex.Message);
                throw;
            }

            return unAdjustedRecords;
        }

        public List<BoxHead> UpdateBoxesAdjusted(List<BoxHead> boxes)
        {
            string sql = "";
            List<BoxHead> adjustedBoxes = new List<BoxHead>();

            sql = "UPDATE FiltersBuilt SET BoxAdjusted = 1 WHERE ID = @filtersBuiltID";

            foreach (BoxHead box in boxes)
            {
                List<Box> adjustedDetails = new List<Box>();
                foreach (Box boxDetail in box.BoxDetails)
                {
                    try
                    {
                        using (IDbConnection DB = createConnection())
                        {
                            DB.Execute(@sql, new
                            {
                                @filtersBuiltID = boxDetail.FiltersBuiltID
                            });
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("NFS_ShopFloorDAL.  Error marking box as adjusted: " + ex.Message);
                    }
                    adjustedDetails.Add(boxDetail);
                }

                BoxHead adjustedBox = new BoxHead();
                adjustedBox.BoxNumber = box.BoxNumber;
                adjustedBox.BoxCount = box.BoxCount;
                adjustedBox.BoxAdjusted = box.BoxAdjusted;
                adjustedBox.BoxDetails = adjustedDetails;

                adjustedBoxes.Add(adjustedBox);
            }

            return adjustedBoxes;
        }
    }
}
