﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using AutomaticBoxAdjustments.Classes;
using NLog;

namespace AutomaticBoxAdjustments
{
    public class DynCustomDAL
    {
        private string DynCustomConnectionString = "";

        /*NLog*/
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public DynCustomDAL()
        {
            try
            {
                    DynCustomConnectionString = AutomaticBoxAdjustments.Properties.Settings.Default.DynCustomConnectionString;
                    logger.Info("DAL. Connection string: " + DynCustomConnectionString);
            }
            catch (Exception ex)
            {
                logger.Error("DAL. Error setting connection string: " + ex.Message);
                throw ex;
            }
        }

        private IDbConnection createConnection()
        {
            try
            {
                if (string.IsNullOrEmpty(DynCustomConnectionString))
                    throw new Exception("The DynCustom Connection string is not set. Please correct.");
            }
            catch (Exception ex)
            {
                logger.Error("DAL. Error creating connection: " + ex.Message);
                throw ex;
            }

            return new SqlConnection(DynCustomConnectionString);
        }

        public int getNextDocumentNumber(int docType, bool development = true, int increment = 1)
        {
            int nextDocumentNumber = 0;

            try
            {
                using (IDbConnection DB = createConnection())
                {
                    if (development == true)
                    {
                        nextDocumentNumber = DB.Query<int>("nfssp_GetNextDocumentNumber_Dev",
                            new
                            {
                                @I_tIVDOCTYPE = docType,
                                @I_tInc_Dec = increment,
                                @O_vIvNumber = 0,
                                @O_iErrorState = 0
                            },
                            commandType: CommandType.StoredProcedure).FirstOrDefault();

                    }
                    else
                    {
                        nextDocumentNumber = DB.Query<int>("nfssp_GetNextDocumentNumber",
                            new
                            {
                                @I_tIVDOCTYPE = docType,
                                @I_tInc_Dec = increment,
                                @O_vIvNumber = 0,
                                @O_iErrorState = 0
                            },
                            commandType: CommandType.StoredProcedure).FirstOrDefault();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return nextDocumentNumber;
        }

        // Updates a documents reason code
        public void UpdateDocumentReasonCode(string adjustmentNumber, string reasonCode, bool development = true)
        {
            try
            {
                using (IDbConnection DB = createConnection())
                {
                    if (development == true)
                    {
                        DB.Execute("nfssp_UpdateDocumentReasonCode_Dev",
                            new
                            {
                                @documentNumber = adjustmentNumber,
                                @reasonCode = reasonCode
                            },
                            commandType: CommandType.StoredProcedure);
                    }
                    else
                    {
                        DB.Execute("nfssp_UpdateDocumentReasonCode",
                            new
                            {
                                @documentNumber = adjustmentNumber,
                                @reasonCode = reasonCode
                            },
                            commandType: CommandType.StoredProcedure);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
