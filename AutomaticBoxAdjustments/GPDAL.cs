﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using AutomaticBoxAdjustments.Classes;
using NLog;
using AutomaticBoxAdjustments.GPService;
using System.ServiceModel;

namespace AutomaticBoxAdjustments
{
    public class GPDAL
    {
        /*NLog*/
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private string gpConnectionString = "";
        private bool liveMode = false;

        public GPDAL(bool LiveMode)
        {
            liveMode = LiveMode;

            if (liveMode)
            {
                gpConnectionString = AutomaticBoxAdjustments.Properties.Settings.Default.GPConnectionString;
            }
            else
            {
                gpConnectionString = AutomaticBoxAdjustments.Properties.Settings.Default.GPDevConnectionString;
            }
        }

        private IDbConnection createConnection()
        {
            if (string.IsNullOrEmpty(gpConnectionString))
                throw new Exception("The GP Connection string is not set. Please correct.");

            return new SqlConnection(gpConnectionString);
        }

        public List<BoxHead> CreateAdjustments(List<BoxHead> boxes)
        {
            List<BoxHead> adjustedBoxes = new List<BoxHead>();
            string adjustmentBox = "";
            int adjustmentQuantity = 0;

            try
            {
                foreach (BoxHead box in boxes)
                {
                    try
                    {
                        bool success = false;

                        if (box.BoxNumber != box.BomBoxNumber)
                        {
                            success = AddAdjustmentToGP(box.BoxNumber, box.BoxCount, !liveMode);


                                BoxHead adjustedBox = box;
                                adjustedBox.BoxAdjusted = true;
                                adjustedBoxes.Add(adjustedBox);

                                success = AddAdjustmentToGP(box.BomBoxNumber, -1 * box.BoxCount, !liveMode);

                                if (success == false)
                                {
                                    logger.Error("*** " + box.BoxCount + " Pack Box " + box.BoxNumber +
                                                 " did not have the " + box.BomBoxCount + " pack box " + box.BomBoxNumber +
                                                 " adjusted for quantity ");
                                }
                                else
                                {
                                logger.Info(box.BoxCount + " Pack Box " + box.BoxNumber +
                                             " did have the " + box.BomBoxCount + " pack box " + box.BomBoxNumber +
                                             " adjusted for quantity ");
                            }
                            }
                            else
                            {
                                BoxHead adjustedBox = box;
                                adjustedBox.BoxAdjusted = true;
                                adjustedBoxes.Add(adjustedBox);
                                logger.Info("*** " + box.BoxCount + " Box " + box.BoxNumber + " was not adjusted for quantity " +
                                             box.BoxCount);
                            }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }

            return adjustedBoxes;
        }

        public bool AddAdjustmentToGP(string itemNumber, int quantity, bool developerMode)
        {
            CompanyKey companyKey;
            Context context;
            BatchKey batchKey;
            ItemKey itemKey;
            Quantity itemQuantity;
            WarehouseKey warehouseKey;
            InventoryKey inventoryKey;
            InventoryAdjustment inventoryAdjustment;
            InventoryLineKey inventoryLineKey;
            InventoryAdjustmentLine inventoryAdjustmentLine;
            Policy inventoryAdjustmentCreatePolicy;
            int nextAdjustmentNumber = 0;
            string AdjustmentString = "Box Adjustment";
            int documentType = 1; // 1 = Adjustment
            int documentIncrementAmount = 1; // Gets the next number
            string ReasonCode = "RELIEVE INVENTO";
            DynCustomDAL dynCustomDAL = new DynCustomDAL();

            try
            {
                // Create an instance of the service
                DynamicsGPClient wsDynamicsGP = new DynamicsGPClient();

                // Create a context with which to call the service
                context = new Context();

                // Specify which company to use (sample company)
                companyKey = new CompanyKey();
                companyKey.Id =
                    ((developerMode)
                        ? 10
                        : 7); // In live mode, use 7 (NFS's Live Company); in development use 10 (NFS's Test Company)

                // Set up the context object
                context.OrganizationKey = (OrganizationKey)companyKey;

                // Gets the next adjustment number
                nextAdjustmentNumber =
                    dynCustomDAL.getNextDocumentNumber(documentType, developerMode,
                        documentIncrementAmount);
                AdjustmentString = nextAdjustmentNumber.ToString();
                if (AdjustmentString.Length == 5)
                {
                    AdjustmentString = "0" + AdjustmentString;
                }

                // Create an inventory key to identify the inventory adjustment object
                inventoryKey = new InventoryKey();
                if (nextAdjustmentNumber != 0)
                {
                    inventoryKey.Id = AdjustmentString;
                }
                else
                {
                    inventoryKey.Id = "Box Adjustment";
                }


                // Create a batch key object to specify a batch for the inventory adjustment
                batchKey = new BatchKey();
                batchKey.Id = "NTBox" + DateTime.Now.ToString("yyyyMMdd");

                // Create the inventory adjustment object
                inventoryAdjustment = new InventoryAdjustment();

                // Populate the inventory adjustment object's required properties
                inventoryAdjustment.Key = inventoryKey;
                inventoryAdjustment.BatchKey = batchKey;
                inventoryAdjustment.Date = DateTime.Today;

                // Create an inventory adjustment line object to detail the inventory adjustment
                inventoryAdjustmentLine = new InventoryAdjustmentLine();

                // Create an inventory line key to identify the inventory line adjustment object
                inventoryLineKey = new InventoryLineKey();
                inventoryLineKey.InventoryKey = inventoryKey;

                // Create an item key object to specify the item
                itemKey = new ItemKey();
                itemKey.Id = itemNumber;

                // Create a quantity object to specify the amount of the adjustment
                itemQuantity = new Quantity();
                itemQuantity.Value = quantity * -1;

                // Create a warehouse key to specify the location of the adjustment
                warehouseKey = new WarehouseKey();
                warehouseKey.Id = "MAIN";

                // Populate the required properties of the inventory line adjustment object
                inventoryAdjustmentLine.Key = inventoryLineKey;
                inventoryAdjustmentLine.ItemKey = itemKey;
                inventoryAdjustmentLine.Quantity = itemQuantity;
                inventoryAdjustmentLine.WarehouseKey = warehouseKey;

                // Create an array to hold the inventory line adjustment object
                InventoryAdjustmentLine[] lines = { inventoryAdjustmentLine };

                // Add the array of inventory adjustment lines to the inventory adjustment object
                inventoryAdjustment.Lines = lines;

                // Get the create policy for an inventory adjustment
                inventoryAdjustmentCreatePolicy = wsDynamicsGP.GetPolicyByOperation(
                    "CreateInventoryAdjustment", context);

                // Create the inventory adjustment
                wsDynamicsGP.CreateInventoryAdjustment(inventoryAdjustment,
                    context, inventoryAdjustmentCreatePolicy);

                // Close the service
                if (wsDynamicsGP.State != CommunicationState.Faulted)
                {
                    wsDynamicsGP.Close();
                }
                else
                {
                    logger.Error("Error adjusting the box.  GP issued error Communication State = Faulted.  Item Number: " + itemNumber + ".  Quantity: " + quantity + ".  Developer Mode: " + developerMode + ".");
                    return false;
                }

                // Set the adjustment reason code
                dynCustomDAL.UpdateDocumentReasonCode(AdjustmentString, ReasonCode,
                    developerMode);
            }
            catch (Exception ex)
            {
                logger.Error("Error adjusting the box.  GP issued error: " + ex.Message + ".  Item Number: " + itemNumber + ".  Quantity: " + quantity + ".  Developer Mode: " + developerMode + "." + Environment.NewLine + "Stack trace: " + ex.StackTrace);
                return false;
            }

            return true;
        }
    }
}
