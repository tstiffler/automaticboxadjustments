﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutomaticBoxAdjustments.Classes
{
    public class Box
    {
        public int FiltersBuiltID { get; set; }
        public string BoxNumber { get; set; }
    }
}
