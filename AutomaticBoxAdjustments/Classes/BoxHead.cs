﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AutomaticBoxAdjustments.Classes
{
    public class BoxHead
    {
        public string BoxNumber { get; set; }
        public int BoxCount { get; set; }
        public bool BoxAdjusted { get; set; }

        public int PackingBoxCount { get; set; }
        public int BomBoxCount { get; set; }
        public string BomBoxNumber { get; set; }

        public List<Box> BoxDetails { get; set; }
    }
}
