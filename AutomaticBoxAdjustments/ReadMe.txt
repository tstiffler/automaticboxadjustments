﻿-- Checks logs
-- C:\Schedule Jobs\Automatic Sales Order Creator for Customs\logs
-- Check that all of the processes succeeded

-- Check for received custom items, change dates to first of the current and next month
DECLARE @StartOfDateRange DateTime
DECLARE @EndOfDateRange DateTime
DECLARE @VendorID int
DECLARE @ShipMethod varchar(50)

SET @StartOfDateRange = '2018-09-01'
SET @EndOfDateRange = '2018-10-01'
SET @VendorID = 10250
SET @ShipMethod = 'BuildAndDeliver'

SELECT 
	'CUSTOMFILTERS' AS GPPartNumber,
	1 AS Quantity,
    MAX(po.id) AS PurchaseOrderID,
	SUM(por.received_qty * pod.cost_each) AS CostEach
FROM purchase_order_received por
INNER JOIN purchase_order_details pod ON pod.id = por.po_detail_id
INNER JOIN purchase_order_head po ON po.id = pod.po_id
WHERE po.vendor_id = @VendorID
AND ship_method = @ShipMethod
AND po_date >= @StartOfDateRange
AND po_date < (DATEADD(d, 1, @EndOfDateRange))
AND ISNULL(po.sent, 0) = 0

-- Check for received custom items, change dates to first of the current and next month

SELECT DISTINCT por.id
FROM purchase_order_received por
INNER JOIN purchase_order_details pod ON pod.id = por.po_detail_id
INNER JOIN purchase_order_head po ON po.id = pod.po_id
WHERE po.vendor_id = @VendorID
AND ship_method = @ShipMethod
AND po_date >= @StartOfDateRange
AND po_date < (DATEADD(d, 1, @EndOfDateRange))
AND ISNULL(po.sent, 0) = 0

-- Check that sales invoices is created
-- GP -> Sales -> Sales Transaction Entry
-- Select Invoice from Type/Type ID drop down
-- Click the magnifying glass by Document No.
-- One invoice should be present.
-- Clicking on it should show one record for item CUSTOMFILTERS

-- E-mail should be sent at this point.
-- Check logs for the sales order number and recipients

-- Check that all purchase orders in the last query above have been marked sent
SELECT id, vendor_id, po_date, ship_method, sent, sent_date, sent_by
FROM purchase_order_head
WHERE po_date >= @StartOfDateRange AND po_date < @EndOfDateRange AND ship_method = 'BuildAndDeliver' AND vendor_id = @VendorID AND ship_method = @ShipMethod

-- Check that vouchers have been created
SELECT *
FROM voucher_head_uncommitted
WHERE vendor_invoice_date >= @StartOfDateRange AND vendor_invoice_date < @EndOfDateRange

-- Check voucher_head created, moves from voucher_head_uncommited every 10 minutes, job Auto Approve Cost Changes, stored procedure sp_approve_paid_costs_vendor
SELECT *
FROM voucher_head
WHERE vendor_invoice_date >= @StartOfDateRange AND vendor_invoice_date < @EndOfDateRange

-- Check that purchase_order_received has been updated with the voucher ID and cost each
SELECT *
FROM purchase_order_received
--WHERE received_date >= @StartOfDateRange AND received_date < @EndOfDateRange
WHERE unc_voucher_id = 104464

-- Check unpaid vendor invoices (vouchers)
-- Dishbooks -> Go -> Unpaid Vendor Invoices
-- Invoice should NOT appear, because it will be paid

-- Check if the vendor invoice has been created (change date if this query to the first day of the month)
SELECT *
FROM voucher_head
WHERE vendor_invoice_date >= '2018-10-01'

-- Get the total for the vendor invoice list above (change the voucher_id to the id from above), query takes a few minutes to run
SELECT isnull((select sum(received_qty * paid_cost_each) from purchase_order_received where voucher_id = vh.id), 0) + isnull((select sum(quantity * fee_amount) from voucher_details where voucher_id = vh.id), 0) as invoice_total
FROM voucher_head vh
INNER JOIN purchase_order_received por ON vh.id = por.voucher_id
WHERE voucher_id IN (104463, 104464

-- Check batch in GP
-- NT Supply
-- GP -> Purchasing -> Transactions -> Batches
-- Click the magnifying glass by Batch ID
-- Click transactions and scroll through them
