﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomaticBoxAdjustments.Classes;
using NLog;

namespace AutomaticBoxAdjustments
{
    public class BoxAdjustmentsBLL
    {
        /*NLog*/
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private bool bllLiveMode = false;

        public BoxAdjustmentsBLL(bool liveMode)
        {
            bllLiveMode = liveMode;
        }

        public List<BoxHead> GetUnAdjustedBoxes(DateTime? startDate, DateTime? endDate)
        {
            List<BoxHead> unAdjustedBoxes = new List<BoxHead>();
            NFS_ShopFloorDAL shopFloorDAL = new NFS_ShopFloorDAL(bllLiveMode);
            DateTime startDateAdjusted = new DateTime();
            DateTime endDateAdjusted = new DateTime();

            try
            {

                if (startDate == null)
                {
                    startDateAdjusted = new DateTime(2019, 01, 01);
                }
                else
                {
                    startDateAdjusted = (DateTime)startDate;
                }

                if (endDate == null)
                {
                    endDateAdjusted = DateTime.Now;
                }
                else
                {
                    endDateAdjusted = (DateTime)endDate;
                }

                unAdjustedBoxes = shopFloorDAL.GetUnAdjustedRecords(startDateAdjusted, endDateAdjusted);
            }
            catch (Exception ex)
            {
                throw;
            }

            return unAdjustedBoxes;
        }

        public List<BoxHead> CreateBoxAdjustment(List<BoxHead> boxes)
        {
            List<BoxHead> adjustedBoxes = new List<BoxHead>();
            GPDAL GPDAL = new GPDAL(bllLiveMode);

            try
            {
                adjustedBoxes = GPDAL.CreateAdjustments(boxes);
            }
            catch (Exception e)
            {
                logger.Error(e);
                throw;
            }

            return adjustedBoxes;
        }

        public List<BoxHead> UpdateBoxesAdjusted(List<BoxHead> boxes)
        {
            List<BoxHead> updatedBoxes = new List<BoxHead>();
            NFS_ShopFloorDAL shopFloorDAL = new NFS_ShopFloorDAL(bllLiveMode);

            updatedBoxes = shopFloorDAL.UpdateBoxesAdjusted(boxes);

            return updatedBoxes;
        }
    }
}
