﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NTSupply.AutomaticSalesInvoiceCreatorForMP;
using NTSupply.Utilities;

namespace UnitTests
{
    [TestClass]
    public class DateRangeToRunUnitTest
    {
        [TestMethod]
        public void Run_NormalWeekStartDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-12");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-11-05").Date, startDateOut.Date);
        }

        [TestMethod]
        public void Run_NormalWeekEndDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-12");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-11-11").Date, endDateOut.Date);
        }

        [TestMethod]
        public void Run_BeginningOfMonthStartDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-05");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-11-01").Date, startDateOut.Date);
        }

        [TestMethod]
        public void Run_BeginningOfMonthEndDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-05");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-11-04").Date, endDateOut.Date);
        }

        [TestMethod]
        public void Run_BeginningOfMonthStartDateTest2()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-12-03");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-12-01").Date, startDateOut.Date);
        }

        [TestMethod]
        public void Run_BeginningOfMonthEndDateTest2()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-12-03");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-12-02").Date, endDateOut.Date);
        }

        [TestMethod]
        public void Run_EndOfMonthStartDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-01");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-10-29").Date, startDateOut.Date);
        }

        [TestMethod]
        public void Run_EndOfMonthEndDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-01");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-10-31").Date, endDateOut.Date);
        }

        [TestMethod]
        public void Run_EndOfMonthStartDateTest2()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-12-01");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-11-26").Date, startDateOut.Date);
        }

        [TestMethod]
        public void Run_EndOfMonthEndDateTest2()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-12-01");

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(null, null, out startDateOut, out endDateOut);

            Assert.AreEqual(ConvertType.ToDateTime("2017-11-30").Date, endDateOut.Date);
        }

        [TestMethod]
        public void Run_DatesGivenStartDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-01");

            DateTime startDate = DateRangeToRun.TodaysDate;
            DateTime endDate = DateRangeToRun.TodaysDate.AddDays(7);

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(startDate, endDate, out startDateOut, out endDateOut);

            Assert.AreEqual(startDate, startDateOut);
        }

        [TestMethod]
        public void Run_DatesGivenEndDateTest()
        {
            DateRangeToRun.TodaysDate = ConvertType.ToDateTime("2017-11-01");

            DateTime startDate = DateRangeToRun.TodaysDate;
            DateTime endDate = DateRangeToRun.TodaysDate.AddDays(7);

            DateTime startDateOut;
            DateTime endDateOut;

            DateRangeToRun.Calculate(startDate, endDate, out startDateOut, out endDateOut);

            Assert.AreEqual(endDate, endDateOut);
        }
    }
}
